*** Settings ***
Documentation     A test suite with a single test for valid login.
...           
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          ../resources.robot

*** Variables ***
${HOST}           https://idp.dev.autentia.systems
${AUTOIDENTIFY}    /authentication/autoidentify
${PASSWORD}       /authentication/password
${PROFILES}       /profiles/
${DATA}=          read_csv_file    ../data.csv
${FACIAL}         /authentication/facial

*** comment ***
FACAL-VALID DATA
                  ${data}=    read_csv_file    ../data.csv
                  ${headers}    Create Dictionary    Accept=application/json    Content-Type=multipart/form-data    charset=utf-8    Authorization=${TOKEN}
                  Create Session    my_session    ${HOST}    verify=false    disable_warnings=1
                  ${count}=    Get length    ${data.DNI}
                  FOR    ${index}    IN RANGE    ${count}
                  ${ruta_foto}    Catenate    SEPARATOR=    ../photos    ${data.DNI[${index}]}    .png
                  ${foto}    Get Binary File    ${ruta_foto}
                  ${payload}    Create Dictionary    rut=${data.DNI[${index}]}    face=${foto}
                  ${response}=    Post Request    my_session    ${FACIAL}    data=${payload}    headers=${headers}
                  ${res_status}=    convert to string    ${response}
                  Run Keyword And Continue On Failure    should contain    ${res_status}    201
                  END
*** Test Cases ***
AUTOIDENTIFY-VALID DNI AND SERIE
    ${data}=    read_csv_file    ../data.csv
    ${headers}    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8    Authorization=${TOKEN}
    Create Session    my_session    ${HOST}    verify=false    disable_warnings=1
    ${count}=    Get length    ${data.DNI}
    FOR    ${index}    IN RANGE    ${count}
        ${payload}    Create Dictionary    sub=${data.DNI[${index}]}    serial=${data.SERIAL[${index}]}
        ${response}=    Post Request    my_session    ${AUTOIDENTIFY}    data=${payload}    headers=${headers}
        ${res_status}=    convert to string    ${response}
        Run Keyword Unless    "${data.SERIAL[${index}]}" == "0"    Run Keyword And Continue On Failure    should contain    ${res_status}    201
    END

AUTOIDENTIFY-VALID DNI AND INVALID SERIAL
    ${data}=    read_csv_file    ../data.csv
    ${headers}    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8    Authorization=${TOKEN}
    Create Session    my_session    ${HOST}    verify=false    disable_warnings=1
    ${count}=    Get length    ${data.DNI}
    FOR    ${index}    IN RANGE    ${count}
        ${payload}    Create Dictionary    sub=${data.DNI[${index}]}    serial=${data.INVALID_DATA[${index}]}
        ${response}=    Post Request    my_session    ${AUTOIDENTIFY}    data=${payload}    headers=${headers}
        ${res_status}=    convert to string    ${response}
        Run Keyword And Continue On Failure    should contain    ${res_status}    400
    END

PASSWORD-VALID DNI AND PASS
    ${data}=    read_csv_file    ../data.csv
    ${headers}    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8    Authorization=${TOKEN}
    Create Session    my_session    ${HOST}    verify=false    disable_warnings=1
    ${count}=    Get length    ${data.DNI}
    FOR    ${index}    IN RANGE    ${count}
        ${payload}    Create Dictionary    rut=${data.DNI[${index}]}    password=${data.PASSWORD[${index}]}
        ${response}=    Post Request    my_session    ${PASSWORD}    data=${payload}    headers=${headers}
        ${res_status}=    convert to string    ${response}
        Run Keyword Unless    "${data.PASSWORD[${index}]}" == "0"    Run Keyword And Continue On Failure    should contain    ${res_status}    201
    END

PASSWORD-VALID DNI AND INVALID PASS
    ${data}=    read_csv_file    ../data.csv
    ${headers}    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8    Authorization=${TOKEN}
    Create Session    my_session    ${HOST}    verify=false    disable_warnings=1
    ${count}=    Get length    ${data.DNI}
    FOR    ${index}    IN RANGE    ${count}
        ${payload}    Create Dictionary    rut=${data.DNI[${index}]}    password=${data.INVALID_DATA[${index}]}
        ${response}=    Post Request    my_session    ${PASSWORD}    data=${payload}    headers=${headers}
        ${res_status}=    convert to string    ${response}
        Run Keyword And Continue On Failure    should contain    ${res_status}    400
    END

SEARCH PROFILES-VALID UUID
    ${data}=    read_csv_file    ../data.csv
    ${headers}    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8    Authorization=${TOKEN}
    Create Session    my_session    ${HOST}    verify=false    disable_warnings=1
    FOR    ${item}    IN    @{data.UUDI}
        ${uri}=    Catenate    SEPARATOR=    ${PROFILES}    ${item}
        ${response}=    Get Request    my_session    ${uri}    headers=${headers}
        ${res_status}=    convert to string    ${response}
        Run Keyword And Continue On Failure    should contain    ${res_status}    200
    END

SEARCH PROFILES-VALID DNI
    ${data}=    read_csv_file    ../data.csv
    ${headers}    Create Dictionary    Accept=application/json    Content-Type=application/json    charset=utf-8    Authorization=${TOKEN}
    ${uri}=    Catenate    SEPARATOR=    ${PROFILES}    /search
    Create Session    my_session    ${HOST}    verify=false    disable_warnings=1
    FOR    ${item}    IN    @{data.DNI}
        ${params}    Create Dictionary    sub=${item}
        ${response}=    Get Request    my_session    ${uri}    params=${params}    headers=${headers}
        ${res_status}=    convert to string    ${response}
        Run Keyword And Continue On Failure    should contain    ${res_status}    200
    END
