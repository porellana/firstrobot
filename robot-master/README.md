# Test_Itrust

## Install Python ENV 3

[pyenv](https://github.com/pyenv/pyenv)

## Install Requirements

```bash
pip install -r requirements.txt
```

## Run Robot

```bash
cd IDP
robot Api_IDP.robot
```

## Robot Framework

[Installation Instructions](https://github.com/robotframework/robotframework/blob/master/INSTALL.rst)

[User guie](http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html)

### Library documentation

[RequestsLibrary](http://marketsquare.github.io/robotframework-requests/doc/RequestsLibrary.html)

[Collections](https://robotframework.org/robotframework/latest/libraries/Collections.html)

[SeleniumLibrary](https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html)

[BuiltIn](https://robotframework.org/robotframework/latest/libraries/BuiltIn.html)

[OperatingSystem](https://robotframework.org/robotframework/latest/libraries/OperatingSystem.html)

Para ejecutar los test de IDP se debe estar dentro de la carpeta(test_itrust/IDP)