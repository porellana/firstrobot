from pandas import read_csv
class csvLibrary(object):

    def read_csv_file(self, filename):

        data = read_csv(filename,dtype=str).fillna(value ='0')
        return data