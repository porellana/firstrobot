*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...           
...               The system specific keywords created here form our own
...               domain specific langu°
Library           RequestsLibrary
Library           json
Library           Collections
Library           csvLibrary.py
Library           OperatingSystem

*** Variables ***
@{VALID DNI}      18925797-K    11607925-9    12178253-7    19412247-0    5883027-5
@{VALID SERIAL}    514914806    A030596606    A024537361
@{VALID PASS}     sKzzS3aSYHxyCN    DRaATgr49piUfD    2JycbkZoFwjr42    MGfvmV23kVhAtF    LbLBt8ePzHzYL7
@{INVALID DATA}    12312sd    A123129312    ${EMPTY}    110377768    A01744112
@{VALID UUID}     f75d1f79-8ab7-5e41-bfa9-d51e7beb7f8d    964d61d8-32c6-4527-b0db-990911adcae2    1f7da069-9179-5d6e-9eb1-60497870920c
@{DNI SERIAL}     ${VALID DNI}    ${VALID SERIAL}
@{DNI INVALID_SERIAL}    ${VALID DNI}    ${INVALID DATA}
@{DNI PASS}       ${VALID DNI}    ${VALID PASS}
@{DNI INVALID_PASS}    ${VALID DNI}    ${INVALID DATA}
${TOKEN}          Bearer aa81bd3bdad7a6955987fe870a8be2f027a2d3794973837ad885cae8c969297fc4beac3c

*** comment ***
valores de strategy para el .flex:nth-child()
1= Estrategia Facial
2= Estrategia Autoidentify
4= Estrategia CUE
5= Estrategia Facial con captura manual
Open Home page Itrust--->Abre la pagina en el ambiente que recibe por argumento(STG o TST)
Got to Strategy Page---->Click en boton de la estrategia enumeradas arriba
*** Keywords ***
Open Home page Itrust
    [Arguments]    ${environment}
    Open Browser    ${URL_${environment}}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}

Got to Strategy Page
    [Arguments]    ${strategy}
    Click Element    css=.flex:nth-child(${strategy}) .v-btn__content
